# spotify_backup

Do a backup of all your playlist and their tracks from Spotify, or just one playlist, to a JSON file.

## Requirements

- Spotipy Python module (installation should take care of it)
- An account in Spotify
- Create your own app in Spotify's developer dashboard https://developer.spotify.com/documentation/general/guides/authorization/app-settings/

## Installation

### Linux

  `sudo python3 setup.py install`

### Windows (from PowerShell)

  `& $(where.exe python).split()[0] setup.py install`

## Usage

Create an app in Spotify's developer dashboard https://developer.spotify.com/documentation/general/guides/authorization/app-settings/ and use the client id and secret to authenticate.
If you don't specify an output file, the output will be printed in the standard output.

  `spotify_backup.py [--debug-level|-d CRITICAL|ERROR|WARNING|INFO|DEBUG|NOTSET] [--log-file <LOG_FILE>] --client-id <CLIENT_ID> --client-secret <CLIENT_SECRET> [--playlist-id <ID>] [--output-file <OUTPUT_FILE>]`
