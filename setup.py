import setuptools
setuptools.setup(
    scripts=['spotify_backup/spotify_backup.py'],
    author="Antonio J. Delgado",
    version='0.0.1',
    name='spotify_backup',
    author_email="",
    url="",
    description="Do backups of your data or public data in Spotify",
    long_description="README.md",
    long_description_content_type="text/markdown",
    license="GPLv3",
    #keywords=["my", "script", "does", "things"]
)
