#!/usr/bin/env python3
# -*- encoding: utf-8 -*-
#
# This script is licensed under GNU GPL version 2.0 or above
# (c) 2022 Antonio J. Delgado
# Do backups of your data or public data in Spotify

import sys
import os
import logging
import click
import click_config_file
from logging.handlers import SysLogHandler
import base64
import json

import spotipy
from spotipy.oauth2 import SpotifyOAuth
class spotify_backup:

    def __init__(self, debug_level, log_file, client_id, client_secret, playlist_id, output_file):
        ''' Initial function called when object is created '''
        self.config = dict()
        self.config['debug_level'] = debug_level
        if log_file is None:
            log_file = os.path.join(os.environ.get('HOME', os.environ.get('USERPROFILE', os.getcwd())), 'log', 'spotify_backup.log')
        self.config['log_file'] = log_file
        self._init_log()
        self._sp_auth(client_id, client_secret)
        your_data = {}
        if playlist_id is None:
            your_data['playlists'] = dict()
            playlists = self._get_all_playlists()
            all_tracks = list()
            for playlist in playlists:
                your_data['playlists'][playlist['id']] = playlist
                all_tracks = all_tracks + self._get_playlist_tracks(playlist['id'])
                your_data['playlists'][playlist['id']]['tracks'] = all_tracks
        else:
            all_tracks = self._get_playlist_tracks(playlist_id)
            your_data['tracks'] = all_tracks
        if output_file:
            with open(output_file, 'w') as output_file_pointer:
                output_file_pointer.write(json.dumps(your_data, indent=2))
        else:
            print(json.dumps(your_data, indent=2))

    def _get_playlist_tracks(self, playlist_id):
        result = self.sp.playlist_tracks(playlist_id)
        total_tracks = result['total']
        limit = result['limit']
        self._log.debug(f"Fetching a total of {total_tracks} in batch of {limit} per request...")
        tracks = list()
        for offset in range(0, int(total_tracks/limit)+1):
            self._log.debug(f"... page {offset+1}/{int(total_tracks/limit)+1}")
            result = self.sp.playlist_tracks(playlist_id, offset=offset*limit)
            tracks = tracks + result['items']

        for track in tracks:
            if track is None:
                self._log.warning("Track is none :S")
            else:
                if 'track' in track and track['track'] is not None:
                    artists = list()
                    if 'artists' in track['track']:
                        for artist in track['track']['artists']:
                            artists.append(artist['name'])
                    track_obj = {
                        "id": track['track']['id'],
                        "artists": artists,
                        "album": track['track']['album']['name'],
                        "name": track['track']['name']
                        }
                else:
                    self._log.warning(f"Track doesn't contain a dictionary called 'track': {track}")
        return tracks

    def _get_all_playlists(self):
        result = self.sp.current_user_playlists()
        total_playlists = result['total']
        limit = result['limit']
        self._log.debug(f"Fetching a total of {total_playlists} in batch of {limit} per request...")
        playlists = list()
        for offset in range(0, int(total_playlists/limit)+1):
            self._log.debug(f"... page {offset+1}/{int(total_playlists/limit)+1}")
            result = self.sp.current_user_playlists(offset=offset*limit)
            playlists = playlists + result['items']
        return playlists

    def _sp_auth(self, client_id, client_secret):
        fall_back_url = 'https://susurrando.com/spotify_backup/callback'
        fall_back_url = 'http://localhost:7766'
        scope = [
            'playlist-read-private',
            'playlist-read-collaborative',
            'user-follow-read',
            'user-read-playback-position',
            'user-top-read',
            'user-read-recently-played',
            'user-library-read',
            'user-read-email',
            'user-read-private',
        ]
        self.sp = spotipy.Spotify(
            auth_manager=SpotifyOAuth(
                client_id=client_id,
                client_secret=client_secret,
                redirect_uri=fall_back_url,
                scope=scope
            )
        )

    def _init_log(self):
        ''' Initialize log object '''
        self._log = logging.getLogger("spotify_backup")
        self._log.setLevel(logging.DEBUG)

        sysloghandler = SysLogHandler()
        sysloghandler.setLevel(logging.DEBUG)
        self._log.addHandler(sysloghandler)

        streamhandler = logging.StreamHandler(sys.stdout)
        streamhandler.setLevel(logging.getLevelName(self.config.get("debug_level", 'INFO')))
        self._log.addHandler(streamhandler)

        if 'log_file' in self.config:
            log_file = self.config['log_file']
        else:
            home_folder = os.environ.get('HOME', os.environ.get('USERPROFILE', ''))
            log_folder = os.path.join(home_folder, "log")
            log_file = os.path.join(log_folder, "spotify_backup.log")

        if not os.path.exists(os.path.dirname(log_file)):
            os.mkdir(os.path.dirname(log_file))

        filehandler = logging.handlers.RotatingFileHandler(log_file, maxBytes=102400000)
        # create formatter
        formatter = logging.Formatter('%(asctime)s %(name)-12s %(levelname)-8s %(message)s')
        filehandler.setFormatter(formatter)
        filehandler.setLevel(logging.DEBUG)
        self._log.addHandler(filehandler)
        return True

@click.command()
@click.option("--debug-level", "-d", default="INFO",
    type=click.Choice(
        ["CRITICAL", "ERROR", "WARNING", "INFO", "DEBUG", "NOTSET"],
        case_sensitive=False,
    ), help='Set the debug level for the standard output.')
@click.option('--log-file', '-l', help="File to store all debug messages.")
#@click.option("--dummy","-n" is_flag=True, help="Don't do anything, just show what would be done.") # Don't forget to add dummy to parameters of main function
@click.option('--client-id', '-c', required=True, help='Client ID of your app in the developers account in Spotify. https://developer.spotify.com/dashboard')
@click.option('--client-secret', '-s', required=True, help='Client secret of your app in the developers account in Spotify. https://developer.spotify.com/dashboard')
@click.option('--playlist-id', '-p', default=None, required=False, help='Playlist ID to get content')
@click.option('--output-file', '-o', required=False, default=None, help='File to write your JSON data')
@click_config_file.configuration_option()
def __main__(debug_level, log_file, client_id, client_secret, playlist_id, output_file):
    return spotify_backup(debug_level, log_file, client_id, client_secret, playlist_id, output_file)

if __name__ == "__main__":
    __main__()

